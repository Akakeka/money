<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Solicitud extends CI_Model {

	private $table = "solicitud_prestamos";
	private $_data = array();

	public function validateSolicitud(){
		$this->load->library('form_validation');
		$this->form_validation->set_rules("SpMontoSolicitado", "Monto Solicitado", "trim|required");
		$this->form_validation->set_rules("IdIntervalo", "Intervalo", "trim|required");
		$this->form_validation->set_rules("Condicion", "Condicion", "trim|required");

		return $this->form_validation->run();
	}

	public function inserSolicitud(){
		$dataCodicion = array(
			'CpCondicion' => $this->input->post('Condicion'),
			'IdIntervalo' => $this->input->post('IdIntervalo')
		);
		$queryCondicion = $this->db->insert('condicion_prestamos', $dataCodicion);
		$last_id = $this->db->insert_id();
		$id = $this->session->userdata('id');
		$dataSolictud= array(
			'SpIdDeudor' => 3,
			'SpIdCondicion' => $last_id,
			'SpEstadoSolicitud' => 1,
			'SpMontoSolicitado' => $this->input->post('SpMontoSolicitado'),
			'SpFechaRegistro' => date("y-m-d H:i:s"),
			'SpFechaEdicion' => date("y-m-d H:i:s")
		);

		$query = $this->db->insert($this->table, $dataSolictud);
		// $last_id = $this->db->insert_id();
		// $this->session->set_userdata($last_id);

		// $sql = $this->db->set($data)->get_compiled_insert($this->table);
		// echo $sql;
	}

	public function get_data()
	{
		return $this->_data;
	}

	public function getSolicitudes(){
		//$query = $this->db->get('user')->result();
		$this->db->select('u.id,u.Nombre,u.Papellido,sp.SpIdSolicitud as idSolicitud,sp.SpMontoSolicitado as Monto,sp.SpEstadoSolicitud,sp.SpFechaRegistro as Fecha,i.Intervalo,cp.CpCondicion');
		$this->db->from('solicitud_prestamos as sp');
		$this->db->join('condicion_prestamos as cp','sp.SpIdCondicion = cp.CpIdCondicion');
		$this->db->join('user as u','u.id = sp.SpIdDeudor');
		$this->db->join('intervalo as i','i.IdIntervalo = cp.IdIntervalo');
		// $this->db->get_compiled_select('solicitud_prestamos',false);
		// $this->db->result_array();
		$query = $this->db->get()->result_array();
		// var_dump($sql) ;
		// exit();
	// $query =	$this->db->get();
	// var_dump($query);
		return $query;
	}

}