<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Empleado extends CI_Model {

	private $table = "user";
	private $_data = array();

	public function validaterRegisterEmpleado(){
		$this->load->library('form_validation');
		$this->form_validation->set_rules("username", "username", "trim|required");
		$this->form_validation->set_rules("email", "email", "trim|required");
		$this->form_validation->set_rules("Nombre", "Nombre", "trim|required");
		$this->form_validation->set_rules("Papellido", "Papellido", "trim|required");
		$this->form_validation->set_rules("Sapellido", "Sapellido", "trim|required");
		$this->form_validation->set_rules("Direccion", "Direccion", "trim|required");
		$this->form_validation->set_rules("password", "contraseña", "trim|required");
		$this->form_validation->set_rules("password2", "Confirmar contraseña", "trim|required|matches[password]");	
		$this->form_validation->set_rules("IdRol", "IdRol", "required");
		
		$result = $this->form_validation->run();
		
		$username = $this->input->post('username');
		$this->db->where("username", $username);
		$query = $this->db->get($this->table);

		if ($result) {
			$status = 200;
		}elseif ($query->num_rows()) {
			$status = 404;
		}else{
			$status = 501;
		}

		// echo $status;
		// exit();
		return $status;
	}

	public function registrarEmpleado(){
		$data = array(
			'username' => $this->input->post('username'),
			'email' => $this->input->post('email'),
			'Nombre' => $this->input->post('Nombre'),
			'Papellido' => $this->input->post('Papellido'),
			'Sapellido' => $this->input->post('Sapellido'),
			'Direccion' => $this->input->post('Direccion'),
			'password' => sha1($this->input->post('password')),
			'IdRol' => $this->input->post('IdRol')

		);
		$query = $this->db->insert($this->table, $data);
		$last_id = $this->db->insert_id();
		$this->session->set_userdata('id',$last_id);
		return $query;
	}

}