<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Auth_model extends CI_Model {

	private $table = "user";
	private $_data = array();

	public function validate()
	{
		$username = $this->input->post('username');
		$password = $this->input->post('password');

		$this->db->where("username", $username);
		$query = $this->db->get($this->table);

		if ($query->num_rows()) 
		{
			// found row by username	
			$row = $query->row_array();

			// now check for the password
			if ($row['password'] == sha1($password)) 
			{
				// we not need password to store in session
				unset($row['password']);
				// die($row['id']);
				$this->session->set_userdata('id', $row['id']);
				// if (isset($_SESSION['id'])) {
				// 	die($_SESSION['id']);
				// }else{
				// 	die('no esta');
				// }
				$this->_data = $row;
				return ERR_NONE;
			}

			// password not match
			return ERR_INVALID_PASSWORD;
		}
		else {
			// not found
			return ERR_INVALID_USERNAME;
		}
	}
	public function validaterRegister(){
		$this->load->library('form_validation');
		$this->form_validation->set_rules("username", "username", "trim|required");
		$this->form_validation->set_rules("email", "email", "trim|required");
		$this->form_validation->set_rules("Nombre", "Nombre", "trim|required");
		$this->form_validation->set_rules("Papellido", "Papellido", "trim|required");
		$this->form_validation->set_rules("Sapellido", "Sapellido", "trim|required");
		$this->form_validation->set_rules("Direccion", "Direccion", "trim|required");
		$this->form_validation->set_rules("password", "contraseña", "trim|required");
		$this->form_validation->set_rules("password2", "Confirmar contraseña", "trim|required|matches[password]");	
		$this->form_validation->set_rules("IdRol", "IdRol", "required");
		$this->form_validation->set_rules("Clave", "Clave", "required");
		if ($this->input->post('Empresa')) {
			// die('entro');
			$this->form_validation->set_rules("Empresa", "Nombre Empresa", "required");
			$this->form_validation->set_rules("Monto", "Monto Tarjeta", "min_length[7]|required");
		}
		

		return $this->form_validation->run();
	}

	public function registrar(){
		$data = array(
			'username' => $this->input->post('username'),
			'email' => $this->input->post('email'),
			'Nombre' => $this->input->post('Nombre'),
			'Papellido' => $this->input->post('Papellido'),
			'Sapellido' => $this->input->post('Sapellido'),
			'Direccion' => $this->input->post('Direccion'),
			'password' => sha1($this->input->post('password')),
			'IdRol' => $this->input->post('IdRol')

		);

		$query = $this->db->insert($this->table, $data);
		$last_id = $this->db->insert_id();
		$this->session->set_userdata('id',$last_id);
		$dataTarjeta = array(
			'IdUsuario' =>$last_id,
			'Clave' => sha1($this->input->post('Clave')),
			'Monto' => ($this->input->post('Monto') ? $this->input->post('Monto'):0),
			'IdRol' => $this->input->post('IdRol')
		);
		
		$query = $this->db->insert('tarjetas', $dataTarjeta);

		if ($this->input->post('Empresa')) {
			$dataEmpresa = array(
				'IdAcreedor' => $last_id,
				'NombreEmpresa' => $this->input->post('Empresa')
			);
			$query = $this->db->insert('empresa', $dataEmpresa);
		}
		
		// $sql = $this->db->set($data)->get_compiled_insert($this->table);
		// echo $sql;
	}

	public function get_data()
	{
		return $this->_data;
	}

}