<div class="container main ">   
 <div class="row login-wrapper panel">
  <div class="col-sm-10 col-sm-offset-1">
    <div class="panel-default">
      <div class="page-header">
        <h1><i class="fa fa-cc-visa">SOLICITUD</i></h1>
      </div>
      <div class="panel-body">
          <div class="col-sm-12 center-block">
              <form name="solicitud" id="solicitud" action="<?php echo site_url("storeSolicitud") ?>" method="post">
                <div class="form-group">
                  <div class="form-row">
                    <?php $error = form_error("SpMontoSolicitado", "<p class='text-danger size-error'>", '</p>'); ?> 
                    <div class="col-sm-6">
                      <label for="SpMontoSolicitado">Valor</label>
                      <input class="form-control" id="SpMontoSolicitado" value="<?php echo set_value("SpMontoSolicitado") ?>" name="SpMontoSolicitado" type="number" aria-describedby="nameHelp" placeholder="SpMontoSolicitado">
                      <?php echo $error; ?>
                    </div>
                    <?php $errorintervalo = form_error("IdIntervalo", "<p class='text-danger size-error'>", '</p>'); ?> 
                    <div class="col-sm-6">
                      <label for="IdIntervalo">Intervalo</label>
                      <!-- <input class="form-control" id="password1" name="password1" type="password" placeholder="Contraseña"> -->
                      <select name="IdIntervalo" id="IdIntervalo" value="<?php echo set_value("IdIntervalo") ?>" class="form-control" >
                        <option value="">Selecciones Rol</option>
                        <option value="1">Diario</option>
                        <option value="2">Semanal</option>
                        <option value="3">Quincenal</option>
                        <option value="4">Mensual</option>
                      </select>
                      <?php echo $errorintervalo; ?>
                    </div>
                    
                  </div>
                </div>
                <div class="form-group">
                  <div class="form-row">
                    <?php $errorCondicion = form_error("Condicion", "<p class='text-danger size-error'>", '</p>'); ?>
                    <div class="col-sm-6">
                      <label for="condicion">Condicion</label>
                      <textarea cols="6" rows="3" class="form-control" name="Condicion" id="Condicion" placeholder="Condicion de prestamos"></textarea>
                      <!-- <input class="form-control" id="password1" name="password1" type="password" placeholder="Contraseña"> -->
                      <?php echo $errorCondicion; ?>
                    </div>
                    
                    <div class="col-sm-6">
                     <!--  <label for="password2">Confirmar Contraseña</label>
                      <input class="form-control" id="password2" name="password2" type="password" placeholder="Confirmar contraseña"> -->
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <div class="form-row">
                    <div class="col-sm-12" style="margin-top: 25px;">
                      <input class="btn btn-success" type="submit" name="register" id="register" class="button" value="Enviar" /> 
                      
                    </div>
                  </div>
                </div>
              </form>
          </div>    
      </div>
    </div>
  </div>
 </div>
</div>