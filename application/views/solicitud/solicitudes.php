<div class="container main">
  <div class="container main card container animated fadeIn">   
    <div class="row login-wrapper panel">
      <div class="col-sm-10 col-sm-offset-1">
        <div class="panel-default">   
        	<div class="page-header">
        		<h1><i class="fa fa-acquisitions-incorporated">SOLICITUD</i></h1>
        	</div>
            <div class="row">
                <!-- <div class="col-sm-2"></div> -->
                <div class="col-sm-12 center-block">
                    <table id="Solicitudes" class="compact display" style="width: 100%;border-color: black;border: 1px;">
                      <thead>
                        <tr>
                          <th>IdSolicitud</th>
                          <th>Nombre</th>
                          <th>Monto</th>
                          <th>Intervalo</th>
                          <th>Fecha</th>
                          <th>Accion</th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php foreach ($titulo as $value): ?>
                          <tr>
                          <td><?php  echo $value['idSolicitud'] ?></td>
                          <td><?php  echo $value['Nombre'] ?></td>
                          <td><?php  echo number_format((int) $value['Monto'])?></td>
                          <td><?php  echo $value['Intervalo'] ?></td>
                          <td><?php  echo $value['Fecha'] ?></td>
                          <td>
                            <div class="buttons">
                              <button class="btn btn-mini btn-primary" data-toggle="modal" data-target="#myModal">
                                  Info
                              </button>
                              <!-- <button class="btn btn-mini btn-success" >
                                  Aprobar
                              </button> -->
                              <a class="btn btn-mini btn-success" href="<?php echo site_url("SolcitudController/resourceSolicitud/5") ?>">Aprobar</a>
                            </div>
                          </td>
                        </tr>
                        <?php endforeach ?>
                        
                      </tbody>
                    </table>
                </div>    
            </div>
            <footer> 
                <h4 class="text-center" style="margin-bottom: 0px;">&copy; 2019 Out Of Money</h4>                  
            </footer>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" tabindex="-1" role="dialog" id="modal-detalle" aria-labelledby="mySmallModalLabel">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
      ...
    </div>
  </div>
</div>

<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Modal title</h4>
      </div>
      <div class="modal-body">
        ...
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div>
    </div>
  </div>
</div>

<!-- <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Modal title</h4>
      </div>
      <div class="modal-body">
        ...
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div>
    </div>
  </div>
</div> -->