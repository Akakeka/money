 <div class="jumbotron jumbotron-fluid">
  <div class="container">
    <div class="card card-register mx-auto mt-5">
      <div class="card-header">
        <div class="col-sm-12">
        <h2 class="form-label">Registra una cuenta</h2>  
        </div>
      </div>
      <div class="card-body">
        <form name="registerform" id="registerform" action="<?php echo site_url("registrarEmpleado") ?>" method="post">
          <div class="form-group">
            <div class="form-row">
              <?php $error1 = $this->session->flashdata("error", "<p class='text-danger size-error'>", '</p>'); ?>
              <?php $error = form_error("username", "<p class='text-danger size-error'>", '</p>'); ?> 
              <div class="col-md-6">
                <label for="username">Username</label>
                <input class="form-control" id="username" value="<?php echo set_value("username") ?>" name="username" type="text" aria-describedby="nameHelp" placeholder="username">
                <?php echo $error; ?>
                <?php echo $error1; ?>
              </div>
              <?php $errorEmail = form_error("email", "<p class='text-danger size-error'>", '</p>'); ?> 
              <div class="col-md-6">
                <label for="email">Email</label>
                <input class="form-control" id="email" name="email" value="<?php echo set_value("email") ?>" type="email" aria-describedby="nameHelp" placeholder="Email">
                <?php echo $errorEmail; ?>
              </div>
              
            </div>
          </div>
          <div class="form-group">
            <div class="form-row">
              <?php $errorNombre = form_error("Nombre", "<p class='text-danger size-error'>", '</p>'); ?>
              <div class="col-md-6">
                <label for="Nombre">Nombres</label>
                <input class="form-control" id="Nombre" name="Nombre" value="<?php echo set_value("Nombre") ?>" type="text" aria-describedby="nameHelp" placeholder="Nombres">
                <?php echo $errorNombre; ?>
              </div>
              
              <?php $errorPapellido = form_error("Papellido", "<p class='text-danger size-error'>", '</p>'); ?>
              <div class="col-md-6">
                <label for="Papellido">Primer Apellidos</label>
                <input class="form-control" id="Papellido" name="Papellido" value="<?php echo set_value("Papellido") ?>" type="text" aria-describedby="nameHelp" placeholder="Primer Apellidos">
                <?php echo $errorPapellido; ?>
              </div>
              
            </div>
          </div>
          <div class="form-group">
            <div class="form-row">
              <?php $errorSapellido = form_error("Sapellido", "<p class='text-danger size-error'>", '</p>'); ?>
              <div class="col-md-6">
                <label for="Sapellido">Segundo Apellido</label>
                <input class="form-control" id="Sapellido" name="Sapellido" value="<?php echo set_value("Sapellido") ?>" type="text" aria-describedby="emailHelp" placeholder="Segundo Apellido">
                <?php echo $errorSapellido; ?>
              </div>
              
              <?php $errorDireccion = form_error("Direccion", "<p class='text-danger size-error'>", '</p>'); ?>
              <div class="col-md-6">
                <label for="Direccion">Direccion</label>
                <input class="form-control" id="Direccion" name="Direccion" value="<?php echo set_value("Direccion") ?>" type="text" aria-describedby="emailHelp" placeholder="Direccion">
                <?php echo $errorDireccion; ?>
              </div>
              
            </div>
          </div>
          <div class="form-group">
            <div class="form-row">
              <?php $errorpassword = form_error("password", "<p class='text-danger size-error'>", '</p>'); ?>
              <div class="col-md-6">
                <label for="password">Contraseña</label>
                <input class="form-control" id="password" name="password" type="password" placeholder="Contraseña">
                <?php echo $errorpassword; ?>
              </div>
              
              <?php $errorpassword2 = form_error("password2", "<p class='text-danger size-error'>", '</p>'); ?>
              <div class="col-md-6">
                <label for="password2">Confirmar Contraseña</label>
                <input class="form-control" id="password2" name="password2" type="password" placeholder="Confirmar contraseña">
                <?php echo $errorpassword2; ?>
              </div>
              
            </div>
          </div>
          <div class="form-group">
            <div class="form-row">
              <?php $errorIdRol = form_error("IdRol", "<p class='text-danger size-error'>", '</p>'); ?>
              <div class="col-md-6">
                <label for="IdRol">Rol</label>
                <!-- <input class="form-control" id="password1" name="password1" type="password" placeholder="Contraseña"> -->
                <select name="IdRol" id="IdRol" value="<?php echo set_value("IdRol") ?>" class="form-control" >
                  <option value="">Selecciones Rol</option>
                  <option value="4">Gerente</option>
                  <option value="5">Tecnico</option>
                </select>
                <?php echo $errorIdRol; ?>
              </div>
              <?php $errorClave = form_error("Clave", "<p class='text-danger size-error'>", '</p>'); ?>
              <div class="col-md-6">
              </div>
             
              
              
            </div>
          </div>
          <div class="form-group">
            <div class="form-row">
              <div class="col-md-12" style="margin-top: 25px;">
                <input class="btn btn-primary" type="submit" name="register" id="register" class="button" value="Guardar" /> 
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
<script>
$(document).ready(function() {
  $('#IdRol').change(function(){
    if ($('#IdRol').val() == 2 ) {
        $('#empresa').show(300);
        // $('#Monto').show(500);
    }else{
        $('#empresa').hide(300);
        // $('#Monto').hide(500);
    }
  })
});
</script>