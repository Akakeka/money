<nav class="navbar navbar-default navbar-fixed-top">
	<div class="container">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
		        <span class="sr-only">Toggle navigation</span>
		        <span class="icon-bar"></span>
		        <span class="icon-bar"></span>
		        <span class="icon-bar"></span>
	      	</button>
	      	<a class="navbar-brand" href="#">Money</a>
		</div>  
  		<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">		  		
	  		<ul class="nav navbar-nav">
		        <li><a href="<?php echo site_url('dashboard') ?>">Dashboard</a></li>
				<li><a href="<?php echo site_url("solicitud") ?>">Prestar</a></li>              
				<li><a href="<?php echo site_url("solicitudes") ?>">Solicitudes</a></li> 
				<li><a href="<?php echo site_url("registerEmpleado") ?>">Empleado</a></li> 
		    </ul>
		    <ul class="nav navbar-nav navbar-right">		        
				<li><a href="<?php echo site_url("AuthController/logout") ?>">logout</a></li> 
		    </ul>
	    </div>
	</div>
</nav>
<!-- <script>
	$(document).ready(function() {
	  $('#IdRol').change(function(){
	    if ($('#IdRol').val() == 2 ) {
	        $('#empresa').show(300);
	        // $('#Monto').show(500);
	    }else{
	        $('#empresa').hide(300);
	        // $('#Monto').hide(500);
	    }
	  })
	});
</script> -->