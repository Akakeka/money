<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * This controller can be accessed 
 * for (all) non logged in users
 */
class AuthController extends MY_Controller {	
	function __construct()
	{
		 parent::__construct();
		// $this->foo = $foo;
		$this->load->library('form_validation');
		$this->load->model('auth_model', 'auth');
		$this->load->library('session');
	}

	public function logged_in_check()
	{
		if ($this->session->userdata("logged_in")) {
			redirect("dashboard");
		}
	}

	public function index()
	{	
		$this->logged_in_check();
		
		$this->load->library('form_validation');
		$this->form_validation->set_rules("username", "Username", "trim|required");
		$this->form_validation->set_rules("password", "Password", "trim|required");
		if ($this->form_validation->run() == true) 
		{
			// $this->load->model('auth_model', 'auth');	
			// check the username & password of user
			$status = $this->auth->validate();
			if ($status == ERR_INVALID_USERNAME) {
				$this->session->set_flashdata("error", "Usuario invalido");
			}
			elseif ($status == ERR_INVALID_PASSWORD) {
				$this->session->set_flashdata("error", "Contraseña invalidad");
			}
			else
			{
				// echo $this->session->userdata('id');
				// exit();
				$this->session->set_userdata('data',$this->auth->get_data());
				$this->session->set_userdata("logged_in", true);
				$data = $this->session->userdata('data');
				// echo $data['IdRol'];
				// var_dump($this->session->userdata('id')) ;
				// exit();
				// redirect("dashboard");
				$this->load->view("macro/header");
				$this->load->view("macro/navbar".$data['IdRol']);
				$this->load->view("dashboard");
				$this->load->view("macro/footer");
			}
		}else{
			$this->load->view("macro/header");		
			$this->load->view("auth");
			$this->load->view("macro/footer");	
		}

		
	}

	public function register(){
		$this->load->view("macro/header");
		$this->load->view('acceso/register');
		$this->load->view("macro/footer");
	}

	public function registrar(){
		// echo "string";
		if($this->auth->validaterRegister()){
			if ($this->auth->registrar()) {
				redirect("dashboard");
			}else{
				$this->load->view("macro/header");
				$this->load->view('acceso/register');
				$this->load->view("macro/footer");
			}
		}else{
			$this->load->view("macro/header");
			$this->load->view('acceso/register');
			$this->load->view("macro/footer");
		}
	}


	public function logout()
	{
		$this->session->unset_userdata("logged_in");
		$this->session->sess_destroy();
		redirect("authController");
	}

    public function resourceSolicitud ($id = null){
        echo $id;
    }

}