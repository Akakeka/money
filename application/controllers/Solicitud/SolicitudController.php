<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class SolicitudController extends CI_Controller {

    function __construct()
    {
         parent::__construct();
        // $this->foo = $foo;
        $this->load->model('Solicitud/solicitud', 'solicitud');
    }

    public function index()
    {
   
        $this->load->view("macro/header");
		$this->load->view("macro/navbar1");
		$this->load->view("solicitud/solicitudPrestamo");
		$this->load->view("macro/footer");
    }

    public function storeSolicitud()
    {
           if($this->solicitud->validateSolicitud()){
                if ($this->solicitud->inserSolicitud()) {
                    redirect("dashboard");
                }else{
                    $this->load->view("macro/header");
                    $this->load->view("macro/navbar1");
                    $this->load->view('solicitud/solicitudPrestamo');
                    $this->load->view("macro/footer");
                }
            }else{
                $this->load->view("macro/header");
                $this->load->view("macro/navbar1");
                $this->load->view('solicitud/solicitudPrestamo');
                $this->load->view("macro/footer");
            }
    }
    public function getSolicitudes(){
        $datos = $this->solicitud->getSolicitudes();
        $datos['titulo'] =  $datos;
        // foreach ($datos as $key => $value) {
        //     var_dump($value);
        //     exit();
        // }
       
        $this->load->view("macro/header");
        $this->load->view("macro/navbar1");
        $this->load->view('solicitud/solicitudes', $datos);
        $this->load->view("macro/footer");
    }
    public function resourceSolicitud ($id){
        echo $id;
    }
}