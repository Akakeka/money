<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * This controller can be accessed 
 * for all logged in users
 */
class Dashboard extends MY_Controller {	

	public function index()
	{
		$this->load->view("macro/header");
		$this->load->view("macro/navbar1");
		$this->load->view("dashboard");
		$this->load->view("macro/footer");
	}

}