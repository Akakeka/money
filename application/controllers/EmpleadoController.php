<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * This controller can be accessed 
 * for (all) non logged in users
 */
class EmpleadoController extends MY_Controller {	
	function __construct()
	{
		 parent::__construct();
		// $this->foo = $foo;
		$this->load->model('Empleado', 'empleado');
	}

	public function registerEmpleado(){
		$this->load->view("macro/header");
		$this->load->view("macro/navbar1");
		$this->load->view('solicitud/crearEmpleado');
		$this->load->view("macro/footer");
	}

	public function registrarEmpleado(){
		// echo "string";
		$status = $this->empleado->validaterRegisterEmpleado();

		// echo $status;
		// exit();
	 	if($status == 200){
			
			if ($this->empleado->registrarEmpleado()) {
				redirect("dashboard");
			}else{
				$this->load->view("macro/header");
				$this->load->view("macro/navbar1");
				$this->load->view('solicitud/crearEmpleado');
				$this->load->view("macro/footer");
			}
		}else if ($status == 404) {
			$this->session->set_flashdata("error", "Usuario ya existe");
			$this->load->view("macro/header");
			$this->load->view("macro/navbar1");
			$this->load->view('solicitud/crearEmpleado');
			$this->load->view("macro/footer");
		}else{
			$this->load->view("macro/header");
			$this->load->view("macro/navbar1");
			$this->load->view('solicitud/crearEmpleado');
			$this->load->view("macro/footer");
		}
		
	}

    public function resourceSolicitud ($id = null){
        echo $id;
    }

}