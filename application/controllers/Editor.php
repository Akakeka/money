<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * This controller can be accessed 
 * for Editor group only
 */
class Editor extends MY_Controller {

	protected $access = array("Admin", "Editor");

	public function index()
	{
		$this->load->view("macro/header");
		$this->load->view("macro/navbar1");
		$this->load->view("editor");
		$this->load->view("macro/footer");
	}
}