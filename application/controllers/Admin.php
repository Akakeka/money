<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * This controller can be accessed 
 * for Admin group only
 */
class Admin extends MY_Controller {

	protected $access = "Admin";
	
	public function index()
	{
		$this->load->view("macro/header");
		$this->load->view("macro/navbar1");
		$this->load->view("macro/admin");
		$this->load->view("macro/footer");
	}

}